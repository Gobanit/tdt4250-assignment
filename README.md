# TDT4250 Assignment

Assignment for [TDT4250](https://www.ntnu.no/studier/emner/TDT4250) course at NTNU.
Repository contains multiple modules. 

## Structure

### StudyProgramModel 
Contains model (**/model/**) and generated source codes (**/src_gen/**). 
Model is represented by **studyProgramModel.ecore** file, while **University.xmi** represents modelled instance.
There is only one custom made function in source codes, it is **getAllCourses()** in class **Semester**.

### StudyProgramModel.edit
TODO

### StudyProgramModel.editor
TODO 

### StudyProgramModel.test
Contains unit tests in */src/* folder. There is only one custom implemented test **testGetAllCourses()** in class **SemesterTest**.

### Accello
Contains project for model to text transformation (.xmi to .html). Should be run with input **/input/University.xmi** and ecore model found in **../StudyProgramModel/model/studyProgramModel.ecore**.

### Model Changes
- Added name to university
- Added code to specialisation for better referencing in html