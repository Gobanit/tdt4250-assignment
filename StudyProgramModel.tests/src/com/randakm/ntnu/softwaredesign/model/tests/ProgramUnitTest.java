/**
 */
package com.randakm.ntnu.softwaredesign.model.tests;

import com.randakm.ntnu.softwaredesign.model.ProgramUnit;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Program Unit</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ProgramUnitTest extends TestCase {

	/**
	 * The fixture for this Program Unit test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProgramUnit fixture = null;

	/**
	 * Constructs a new Program Unit test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProgramUnitTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Program Unit test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ProgramUnit fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Program Unit test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProgramUnit getFixture() {
		return fixture;
	}

} //ProgramUnitTest
