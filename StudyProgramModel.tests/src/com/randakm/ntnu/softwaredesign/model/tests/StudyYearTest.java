/**
 */
package com.randakm.ntnu.softwaredesign.model.tests;

import com.randakm.ntnu.softwaredesign.model.StudyProgramModelFactory;
import com.randakm.ntnu.softwaredesign.model.StudyYear;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Study Year</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class StudyYearTest extends ProgramUnitTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StudyYearTest.class);
	}

	/**
	 * Constructs a new Study Year test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyYearTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Study Year test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected StudyYear getFixture() {
		return (StudyYear) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StudyProgramModelFactory.eINSTANCE.createStudyYear());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //StudyYearTest
