/**
 */
package com.randakm.ntnu.softwaredesign.model.tests;

import com.randakm.ntnu.softwaredesign.model.Course;
import com.randakm.ntnu.softwaredesign.model.ElectiveGroup;
import com.randakm.ntnu.softwaredesign.model.Semester;
import com.randakm.ntnu.softwaredesign.model.StudyProgramModelFactory;

import junit.framework.TestCase;
import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Semester#getAllCourses() <em>All Courses</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class SemesterTest extends TestCase {

	/**
	 * The fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Semester fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SemesterTest.class);
	}

	/**
	 * Constructs a new Semester test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemesterTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Semester fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Semester getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StudyProgramModelFactory.eINSTANCE.createSemester());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link com.randakm.ntnu.softwaredesign.model.Semester#getAllCourses() <em>All Courses</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.randakm.ntnu.softwaredesign.model.Semester#getAllCourses()
	 * @generated NOT
	 */
	public void testGetAllCourses() {
		StudyProgramModelFactory factory = StudyProgramModelFactory.eINSTANCE;
		Semester s = factory.createSemester();
		
		Course c1 = factory.createCourse();
		Course c2 = factory.createCourse();
		
		s.getMandatoryCourses().add(c1);
		s.getMandatoryCourses().add(c2);
		
		assertTrue(s.getAllCourses().size() == 2);
		
		Course c3 = factory.createCourse();
		s.getElectiveCourses().add(c3);
		
		assertTrue(s.getAllCourses().size() == 3);
		
		Course c4 = factory.createCourse();
		ElectiveGroup eg = factory.createElectiveGroup();
		eg.getCourses().add(c4);
		s.getElectiveCourseGroups().add(eg);
		
		assertTrue(s.getAllCourses().size() == 4);

	}

} //SemesterTest
