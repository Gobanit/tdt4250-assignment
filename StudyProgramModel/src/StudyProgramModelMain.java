import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.randakm.ntnu.softwaredesign.model.Course;
import com.randakm.ntnu.softwaredesign.model.CourseCatalogue;
import com.randakm.ntnu.softwaredesign.model.SpecialisationCatalogue;
import com.randakm.ntnu.softwaredesign.model.StudyProgramModelFactory;
import com.randakm.ntnu.softwaredesign.model.University;
import com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelFactoryImpl;

public class StudyProgramModelMain {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		StudyProgramModelFactory fact = new StudyProgramModelFactoryImpl();
		ResourceSet resSet = new ResourceSetImpl();
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("studyProgramModel", fact);

//		load(fact, resSet);

//		save(fact, resSet);
		
		jsoupSandbox();

	}

	private static void save(StudyProgramModelFactory fact, ResourceSet resSet) throws IOException {
		// Resource resource = new XMIResourceImpl();
		Resource resource = new XMIResourceImpl(URI.createFileURI("./test.studyProgramModel"));

		University u = fact.createUniversity();
		CourseCatalogue cc = fact.createCourseCatalogue();
		Course c = fact.createCourse();
		c.setCode("ABCDEF");
		c.setCredits(5.5f);
		c.setName("Abcd Efghi");
		cc.getCourses().add(c);
		u.setCatalogue(cc);

		SpecialisationCatalogue sc = fact.createSpecialisationCatalogue();
		u.setSpecialisationCatalogue(sc);

		resource.getContents().add(u);

		resource.save(null);

	}

	private static void load(StudyProgramModelFactory fact, ResourceSet resSet)
			throws FileNotFoundException, IOException {
		Resource resource = new XMIResourceImpl();

		resource.load(new FileInputStream(new File("model/University.xmi")), null);

		University uni = (University) resource.getContents().get(0);
		System.out.println(uni);

		// resource.save(new FileOutputStream(new File("output.txt")), null);
		// Xhtml1XMLProcessor proc = new Xhtml1XMLProcessor();

		resource.save(new FileOutputStream(new File("./output.txt")), null);

	}
	
	static void jsoupSandbox() {
		Document doc = new Document("./jsoup.html");
		
		doc.appendChild(doc.createElement("head"));
		doc.appendChild(doc.createElement("body"));
		
		Element head = doc.head();
		Element body = doc.body();
		
		Element h1 = doc.createElement("h1");
		h1.append("Hello World!!!");
		
		body.appendChild(h1);
		
		String html = doc.html();
		System.out.println(html);
	}
}
