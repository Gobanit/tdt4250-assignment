/**
 */
package com.randakm.ntnu.softwaredesign.model.util;

import com.randakm.ntnu.softwaredesign.model.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage
 * @generated
 */
public class StudyProgramModelValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final StudyProgramModelValidator INSTANCE = new StudyProgramModelValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "com.randakm.ntnu.softwaredesign.model";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyProgramModelValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
		return StudyProgramModelPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		switch (classifierID) {
		case StudyProgramModelPackage.UNIVERSITY:
			return validateUniversity((University) value, diagnostics, context);
		case StudyProgramModelPackage.PROGRAM:
			return validateProgram((Program) value, diagnostics, context);
		case StudyProgramModelPackage.COURSE_CATALOGUE:
			return validateCourseCatalogue((CourseCatalogue) value, diagnostics, context);
		case StudyProgramModelPackage.SPECIALISATION_CATALOGUE:
			return validateSpecialisationCatalogue((SpecialisationCatalogue) value, diagnostics, context);
		case StudyProgramModelPackage.PROGRAM_UNIT:
			return validateProgramUnit((ProgramUnit) value, diagnostics, context);
		case StudyProgramModelPackage.SPECIALISATION_CHOOSER:
			return validateSpecialisationChooser((SpecialisationChooser) value, diagnostics, context);
		case StudyProgramModelPackage.STUDY_YEAR:
			return validateStudyYear((StudyYear) value, diagnostics, context);
		case StudyProgramModelPackage.SPECIALISATION:
			return validateSpecialisation((Specialisation) value, diagnostics, context);
		case StudyProgramModelPackage.SEMESTER:
			return validateSemester((Semester) value, diagnostics, context);
		case StudyProgramModelPackage.COURSE:
			return validateCourse((Course) value, diagnostics, context);
		case StudyProgramModelPackage.ELECTIVE_GROUP:
			return validateElectiveGroup((ElectiveGroup) value, diagnostics, context);
		case StudyProgramModelPackage.SEMESTER_TYPE:
			return validateSemesterType((SemesterType) value, diagnostics, context);
		default:
			return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUniversity(University university, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(university, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProgram(Program program, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(program, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseCatalogue(CourseCatalogue courseCatalogue, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(courseCatalogue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSpecialisationCatalogue(SpecialisationCatalogue specialisationCatalogue,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(specialisationCatalogue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProgramUnit(ProgramUnit programUnit, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(programUnit, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSpecialisationChooser(SpecialisationChooser specialisationChooser,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(specialisationChooser, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStudyYear(StudyYear studyYear, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(studyYear, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(studyYear, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(studyYear, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(studyYear, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(studyYear, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(studyYear, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(studyYear, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(studyYear, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(studyYear, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateStudyYear_autummSemesterType(studyYear, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateStudyYear_springSemesterType(studyYear, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the autummSemesterType constraint of '<em>Study Year</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String STUDY_YEAR__AUTUMM_SEMESTER_TYPE__EEXPRESSION = "self.autumm.semesterType=SemesterType::Autumm";

	/**
	 * Validates the autummSemesterType constraint of '<em>Study Year</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStudyYear_autummSemesterType(StudyYear studyYear, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(StudyProgramModelPackage.Literals.STUDY_YEAR, studyYear, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL", "autummSemesterType",
				STUDY_YEAR__AUTUMM_SEMESTER_TYPE__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the springSemesterType constraint of '<em>Study Year</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String STUDY_YEAR__SPRING_SEMESTER_TYPE__EEXPRESSION = "self.spring.semesterType=SemesterType::Spring";

	/**
	 * Validates the springSemesterType constraint of '<em>Study Year</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStudyYear_springSemesterType(StudyYear studyYear, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(StudyProgramModelPackage.Literals.STUDY_YEAR, studyYear, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL", "springSemesterType",
				STUDY_YEAR__SPRING_SEMESTER_TYPE__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSpecialisation(Specialisation specialisation, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(specialisation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSemester(Semester semester, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(semester, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(course, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateElectiveGroup(ElectiveGroup electiveGroup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(electiveGroup, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSemesterType(SemesterType semesterType, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //StudyProgramModelValidator
