/**
 */
package com.randakm.ntnu.softwaredesign.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Specialisation Catalogue</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.SpecialisationCatalogue#getSpecialisations <em>Specialisations</em>}</li>
 * </ul>
 *
 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getSpecialisationCatalogue()
 * @model
 * @generated
 */
public interface SpecialisationCatalogue extends EObject {
	/**
	 * Returns the value of the '<em><b>Specialisations</b></em>' containment reference list.
	 * The list contents are of type {@link com.randakm.ntnu.softwaredesign.model.Specialisation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialisations</em>' containment reference list.
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getSpecialisationCatalogue_Specialisations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Specialisation> getSpecialisations();

} // SpecialisationCatalogue
