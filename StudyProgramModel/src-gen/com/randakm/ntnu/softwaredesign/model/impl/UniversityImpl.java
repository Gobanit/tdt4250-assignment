/**
 */
package com.randakm.ntnu.softwaredesign.model.impl;

import com.randakm.ntnu.softwaredesign.model.CourseCatalogue;
import com.randakm.ntnu.softwaredesign.model.Program;
import com.randakm.ntnu.softwaredesign.model.SpecialisationCatalogue;
import com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage;
import com.randakm.ntnu.softwaredesign.model.University;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>University</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.impl.UniversityImpl#getCatalogue <em>Catalogue</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.impl.UniversityImpl#getPrograms <em>Programs</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.impl.UniversityImpl#getSpecialisationCatalogue <em>Specialisation Catalogue</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UniversityImpl extends MinimalEObjectImpl.Container implements University {
	/**
	 * The cached value of the '{@link #getCatalogue() <em>Catalogue</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCatalogue()
	 * @generated
	 * @ordered
	 */
	protected CourseCatalogue catalogue;

	/**
	 * The cached value of the '{@link #getPrograms() <em>Programs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrograms()
	 * @generated
	 * @ordered
	 */
	protected EList<Program> programs;

	/**
	 * The cached value of the '{@link #getSpecialisationCatalogue() <em>Specialisation Catalogue</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialisationCatalogue()
	 * @generated
	 * @ordered
	 */
	protected SpecialisationCatalogue specialisationCatalogue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UniversityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StudyProgramModelPackage.Literals.UNIVERSITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CourseCatalogue getCatalogue() {
		return catalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCatalogue(CourseCatalogue newCatalogue, NotificationChain msgs) {
		CourseCatalogue oldCatalogue = catalogue;
		catalogue = newCatalogue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					StudyProgramModelPackage.UNIVERSITY__CATALOGUE, oldCatalogue, newCatalogue);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCatalogue(CourseCatalogue newCatalogue) {
		if (newCatalogue != catalogue) {
			NotificationChain msgs = null;
			if (catalogue != null)
				msgs = ((InternalEObject) catalogue).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - StudyProgramModelPackage.UNIVERSITY__CATALOGUE, null, msgs);
			if (newCatalogue != null)
				msgs = ((InternalEObject) newCatalogue).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - StudyProgramModelPackage.UNIVERSITY__CATALOGUE, null, msgs);
			msgs = basicSetCatalogue(newCatalogue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyProgramModelPackage.UNIVERSITY__CATALOGUE,
					newCatalogue, newCatalogue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Program> getPrograms() {
		if (programs == null) {
			programs = new EObjectContainmentWithInverseEList<Program>(Program.class, this,
					StudyProgramModelPackage.UNIVERSITY__PROGRAMS, StudyProgramModelPackage.PROGRAM__UNIVERSITY);
		}
		return programs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SpecialisationCatalogue getSpecialisationCatalogue() {
		return specialisationCatalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSpecialisationCatalogue(SpecialisationCatalogue newSpecialisationCatalogue,
			NotificationChain msgs) {
		SpecialisationCatalogue oldSpecialisationCatalogue = specialisationCatalogue;
		specialisationCatalogue = newSpecialisationCatalogue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					StudyProgramModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE, oldSpecialisationCatalogue,
					newSpecialisationCatalogue);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSpecialisationCatalogue(SpecialisationCatalogue newSpecialisationCatalogue) {
		if (newSpecialisationCatalogue != specialisationCatalogue) {
			NotificationChain msgs = null;
			if (specialisationCatalogue != null)
				msgs = ((InternalEObject) specialisationCatalogue).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - StudyProgramModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE, null,
						msgs);
			if (newSpecialisationCatalogue != null)
				msgs = ((InternalEObject) newSpecialisationCatalogue).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - StudyProgramModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE, null,
						msgs);
			msgs = basicSetSpecialisationCatalogue(newSpecialisationCatalogue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					StudyProgramModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE, newSpecialisationCatalogue,
					newSpecialisationCatalogue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case StudyProgramModelPackage.UNIVERSITY__PROGRAMS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getPrograms()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case StudyProgramModelPackage.UNIVERSITY__CATALOGUE:
			return basicSetCatalogue(null, msgs);
		case StudyProgramModelPackage.UNIVERSITY__PROGRAMS:
			return ((InternalEList<?>) getPrograms()).basicRemove(otherEnd, msgs);
		case StudyProgramModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE:
			return basicSetSpecialisationCatalogue(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case StudyProgramModelPackage.UNIVERSITY__CATALOGUE:
			return getCatalogue();
		case StudyProgramModelPackage.UNIVERSITY__PROGRAMS:
			return getPrograms();
		case StudyProgramModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE:
			return getSpecialisationCatalogue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case StudyProgramModelPackage.UNIVERSITY__CATALOGUE:
			setCatalogue((CourseCatalogue) newValue);
			return;
		case StudyProgramModelPackage.UNIVERSITY__PROGRAMS:
			getPrograms().clear();
			getPrograms().addAll((Collection<? extends Program>) newValue);
			return;
		case StudyProgramModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE:
			setSpecialisationCatalogue((SpecialisationCatalogue) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case StudyProgramModelPackage.UNIVERSITY__CATALOGUE:
			setCatalogue((CourseCatalogue) null);
			return;
		case StudyProgramModelPackage.UNIVERSITY__PROGRAMS:
			getPrograms().clear();
			return;
		case StudyProgramModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE:
			setSpecialisationCatalogue((SpecialisationCatalogue) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case StudyProgramModelPackage.UNIVERSITY__CATALOGUE:
			return catalogue != null;
		case StudyProgramModelPackage.UNIVERSITY__PROGRAMS:
			return programs != null && !programs.isEmpty();
		case StudyProgramModelPackage.UNIVERSITY__SPECIALISATION_CATALOGUE:
			return specialisationCatalogue != null;
		}
		return super.eIsSet(featureID);
	}

} //UniversityImpl
