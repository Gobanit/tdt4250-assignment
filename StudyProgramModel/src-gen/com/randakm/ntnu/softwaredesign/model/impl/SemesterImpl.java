/**
 */
package com.randakm.ntnu.softwaredesign.model.impl;

import com.randakm.ntnu.softwaredesign.model.Course;
import com.randakm.ntnu.softwaredesign.model.ElectiveGroup;
import com.randakm.ntnu.softwaredesign.model.Semester;
import com.randakm.ntnu.softwaredesign.model.SemesterType;
import com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.impl.SemesterImpl#getSemesterType <em>Semester Type</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.impl.SemesterImpl#getMandatoryCourses <em>Mandatory Courses</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.impl.SemesterImpl#getElectiveCourses <em>Elective Courses</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.impl.SemesterImpl#getElectiveCourseGroups <em>Elective Course Groups</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.impl.SemesterImpl#getAllCourses <em>All Courses</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SemesterImpl extends MinimalEObjectImpl.Container implements Semester {
	/**
	 * The default value of the '{@link #getSemesterType() <em>Semester Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterType()
	 * @generated
	 * @ordered
	 */
	protected static final SemesterType SEMESTER_TYPE_EDEFAULT = SemesterType.SPRING;

	/**
	 * The cached value of the '{@link #getSemesterType() <em>Semester Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterType()
	 * @generated
	 * @ordered
	 */
	protected SemesterType semesterType = SEMESTER_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMandatoryCourses() <em>Mandatory Courses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMandatoryCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> mandatoryCourses;

	/**
	 * The cached value of the '{@link #getElectiveCourses() <em>Elective Courses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElectiveCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> electiveCourses;

	/**
	 * The cached value of the '{@link #getElectiveCourseGroups() <em>Elective Course Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElectiveCourseGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<ElectiveGroup> electiveCourseGroups;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SemesterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StudyProgramModelPackage.Literals.SEMESTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SemesterType getSemesterType() {
		return semesterType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSemesterType(SemesterType newSemesterType) {
		SemesterType oldSemesterType = semesterType;
		semesterType = newSemesterType == null ? SEMESTER_TYPE_EDEFAULT : newSemesterType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyProgramModelPackage.SEMESTER__SEMESTER_TYPE,
					oldSemesterType, semesterType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Course> getMandatoryCourses() {
		if (mandatoryCourses == null) {
			mandatoryCourses = new EObjectResolvingEList<Course>(Course.class, this,
					StudyProgramModelPackage.SEMESTER__MANDATORY_COURSES);
		}
		return mandatoryCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Course> getElectiveCourses() {
		if (electiveCourses == null) {
			electiveCourses = new EObjectResolvingEList<Course>(Course.class, this,
					StudyProgramModelPackage.SEMESTER__ELECTIVE_COURSES);
		}
		return electiveCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ElectiveGroup> getElectiveCourseGroups() {
		if (electiveCourseGroups == null) {
			electiveCourseGroups = new EObjectContainmentEList<ElectiveGroup>(ElectiveGroup.class, this,
					StudyProgramModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS);
		}
		return electiveCourseGroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Course> getAllCourses() {
		EList<Course> courses = new BasicEList<Course>();
		if(mandatoryCourses != null) courses.addAll(mandatoryCourses);
		if(electiveCourses != null) courses.addAll(electiveCourses);
		if(electiveCourseGroups != null) {
			for (ElectiveGroup eg : electiveCourseGroups) {
				courses.addAll(eg.getCourses());
			}
		}

		return courses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case StudyProgramModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS:
			return ((InternalEList<?>) getElectiveCourseGroups()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case StudyProgramModelPackage.SEMESTER__SEMESTER_TYPE:
			return getSemesterType();
		case StudyProgramModelPackage.SEMESTER__MANDATORY_COURSES:
			return getMandatoryCourses();
		case StudyProgramModelPackage.SEMESTER__ELECTIVE_COURSES:
			return getElectiveCourses();
		case StudyProgramModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS:
			return getElectiveCourseGroups();
		case StudyProgramModelPackage.SEMESTER__ALL_COURSES:
			return getAllCourses();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case StudyProgramModelPackage.SEMESTER__SEMESTER_TYPE:
			setSemesterType((SemesterType) newValue);
			return;
		case StudyProgramModelPackage.SEMESTER__MANDATORY_COURSES:
			getMandatoryCourses().clear();
			getMandatoryCourses().addAll((Collection<? extends Course>) newValue);
			return;
		case StudyProgramModelPackage.SEMESTER__ELECTIVE_COURSES:
			getElectiveCourses().clear();
			getElectiveCourses().addAll((Collection<? extends Course>) newValue);
			return;
		case StudyProgramModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS:
			getElectiveCourseGroups().clear();
			getElectiveCourseGroups().addAll((Collection<? extends ElectiveGroup>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case StudyProgramModelPackage.SEMESTER__SEMESTER_TYPE:
			setSemesterType(SEMESTER_TYPE_EDEFAULT);
			return;
		case StudyProgramModelPackage.SEMESTER__MANDATORY_COURSES:
			getMandatoryCourses().clear();
			return;
		case StudyProgramModelPackage.SEMESTER__ELECTIVE_COURSES:
			getElectiveCourses().clear();
			return;
		case StudyProgramModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS:
			getElectiveCourseGroups().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case StudyProgramModelPackage.SEMESTER__SEMESTER_TYPE:
			return semesterType != SEMESTER_TYPE_EDEFAULT;
		case StudyProgramModelPackage.SEMESTER__MANDATORY_COURSES:
			return mandatoryCourses != null && !mandatoryCourses.isEmpty();
		case StudyProgramModelPackage.SEMESTER__ELECTIVE_COURSES:
			return electiveCourses != null && !electiveCourses.isEmpty();
		case StudyProgramModelPackage.SEMESTER__ELECTIVE_COURSE_GROUPS:
			return electiveCourseGroups != null && !electiveCourseGroups.isEmpty();
		case StudyProgramModelPackage.SEMESTER__ALL_COURSES:
			return !getAllCourses().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (semesterType: ");
		result.append(semesterType);
		result.append(')');
		return result.toString();
	}

} //SemesterImpl
