/**
 */
package com.randakm.ntnu.softwaredesign.model.impl;

import com.randakm.ntnu.softwaredesign.model.Semester;
import com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage;
import com.randakm.ntnu.softwaredesign.model.StudyYear;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Study Year</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.impl.StudyYearImpl#getAutumm <em>Autumm</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.impl.StudyYearImpl#getSpring <em>Spring</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StudyYearImpl extends ProgramUnitImpl implements StudyYear {
	/**
	 * The cached value of the '{@link #getAutumm() <em>Autumm</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAutumm()
	 * @generated
	 * @ordered
	 */
	protected Semester autumm;

	/**
	 * The cached value of the '{@link #getSpring() <em>Spring</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpring()
	 * @generated
	 * @ordered
	 */
	protected Semester spring;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudyYearImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StudyProgramModelPackage.Literals.STUDY_YEAR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Semester getAutumm() {
		return autumm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAutumm(Semester newAutumm, NotificationChain msgs) {
		Semester oldAutumm = autumm;
		autumm = newAutumm;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					StudyProgramModelPackage.STUDY_YEAR__AUTUMM, oldAutumm, newAutumm);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAutumm(Semester newAutumm) {
		if (newAutumm != autumm) {
			NotificationChain msgs = null;
			if (autumm != null)
				msgs = ((InternalEObject) autumm).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - StudyProgramModelPackage.STUDY_YEAR__AUTUMM, null, msgs);
			if (newAutumm != null)
				msgs = ((InternalEObject) newAutumm).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - StudyProgramModelPackage.STUDY_YEAR__AUTUMM, null, msgs);
			msgs = basicSetAutumm(newAutumm, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyProgramModelPackage.STUDY_YEAR__AUTUMM,
					newAutumm, newAutumm));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Semester getSpring() {
		return spring;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSpring(Semester newSpring, NotificationChain msgs) {
		Semester oldSpring = spring;
		spring = newSpring;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					StudyProgramModelPackage.STUDY_YEAR__SPRING, oldSpring, newSpring);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSpring(Semester newSpring) {
		if (newSpring != spring) {
			NotificationChain msgs = null;
			if (spring != null)
				msgs = ((InternalEObject) spring).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - StudyProgramModelPackage.STUDY_YEAR__SPRING, null, msgs);
			if (newSpring != null)
				msgs = ((InternalEObject) newSpring).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - StudyProgramModelPackage.STUDY_YEAR__SPRING, null, msgs);
			msgs = basicSetSpring(newSpring, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyProgramModelPackage.STUDY_YEAR__SPRING,
					newSpring, newSpring));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case StudyProgramModelPackage.STUDY_YEAR__AUTUMM:
			return basicSetAutumm(null, msgs);
		case StudyProgramModelPackage.STUDY_YEAR__SPRING:
			return basicSetSpring(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case StudyProgramModelPackage.STUDY_YEAR__AUTUMM:
			return getAutumm();
		case StudyProgramModelPackage.STUDY_YEAR__SPRING:
			return getSpring();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case StudyProgramModelPackage.STUDY_YEAR__AUTUMM:
			setAutumm((Semester) newValue);
			return;
		case StudyProgramModelPackage.STUDY_YEAR__SPRING:
			setSpring((Semester) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case StudyProgramModelPackage.STUDY_YEAR__AUTUMM:
			setAutumm((Semester) null);
			return;
		case StudyProgramModelPackage.STUDY_YEAR__SPRING:
			setSpring((Semester) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case StudyProgramModelPackage.STUDY_YEAR__AUTUMM:
			return autumm != null;
		case StudyProgramModelPackage.STUDY_YEAR__SPRING:
			return spring != null;
		}
		return super.eIsSet(featureID);
	}

} //StudyYearImpl
