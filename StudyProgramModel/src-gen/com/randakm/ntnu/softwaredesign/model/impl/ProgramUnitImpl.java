/**
 */
package com.randakm.ntnu.softwaredesign.model.impl;

import com.randakm.ntnu.softwaredesign.model.ProgramUnit;
import com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Program Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ProgramUnitImpl extends MinimalEObjectImpl.Container implements ProgramUnit {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProgramUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StudyProgramModelPackage.Literals.PROGRAM_UNIT;
	}

} //ProgramUnitImpl
