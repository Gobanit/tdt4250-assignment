/**
 */
package com.randakm.ntnu.softwaredesign.model.impl;

import com.randakm.ntnu.softwaredesign.model.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StudyProgramModelFactoryImpl extends EFactoryImpl implements StudyProgramModelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StudyProgramModelFactory init() {
		try {
			StudyProgramModelFactory theStudyProgramModelFactory = (StudyProgramModelFactory) EPackage.Registry.INSTANCE
					.getEFactory(StudyProgramModelPackage.eNS_URI);
			if (theStudyProgramModelFactory != null) {
				return theStudyProgramModelFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StudyProgramModelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyProgramModelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case StudyProgramModelPackage.UNIVERSITY:
			return createUniversity();
		case StudyProgramModelPackage.PROGRAM:
			return createProgram();
		case StudyProgramModelPackage.COURSE_CATALOGUE:
			return createCourseCatalogue();
		case StudyProgramModelPackage.SPECIALISATION_CATALOGUE:
			return createSpecialisationCatalogue();
		case StudyProgramModelPackage.SPECIALISATION_CHOOSER:
			return createSpecialisationChooser();
		case StudyProgramModelPackage.STUDY_YEAR:
			return createStudyYear();
		case StudyProgramModelPackage.SPECIALISATION:
			return createSpecialisation();
		case StudyProgramModelPackage.SEMESTER:
			return createSemester();
		case StudyProgramModelPackage.COURSE:
			return createCourse();
		case StudyProgramModelPackage.ELECTIVE_GROUP:
			return createElectiveGroup();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case StudyProgramModelPackage.SEMESTER_TYPE:
			return createSemesterTypeFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case StudyProgramModelPackage.SEMESTER_TYPE:
			return convertSemesterTypeToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public University createUniversity() {
		UniversityImpl university = new UniversityImpl();
		return university;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Program createProgram() {
		ProgramImpl program = new ProgramImpl();
		return program;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CourseCatalogue createCourseCatalogue() {
		CourseCatalogueImpl courseCatalogue = new CourseCatalogueImpl();
		return courseCatalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SpecialisationCatalogue createSpecialisationCatalogue() {
		SpecialisationCatalogueImpl specialisationCatalogue = new SpecialisationCatalogueImpl();
		return specialisationCatalogue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SpecialisationChooser createSpecialisationChooser() {
		SpecialisationChooserImpl specialisationChooser = new SpecialisationChooserImpl();
		return specialisationChooser;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StudyYear createStudyYear() {
		StudyYearImpl studyYear = new StudyYearImpl();
		return studyYear;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Specialisation createSpecialisation() {
		SpecialisationImpl specialisation = new SpecialisationImpl();
		return specialisation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Semester createSemester() {
		SemesterImpl semester = new SemesterImpl();
		return semester;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Course createCourse() {
		CourseImpl course = new CourseImpl();
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ElectiveGroup createElectiveGroup() {
		ElectiveGroupImpl electiveGroup = new ElectiveGroupImpl();
		return electiveGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemesterType createSemesterTypeFromString(EDataType eDataType, String initialValue) {
		SemesterType result = SemesterType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSemesterTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StudyProgramModelPackage getStudyProgramModelPackage() {
		return (StudyProgramModelPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StudyProgramModelPackage getPackage() {
		return StudyProgramModelPackage.eINSTANCE;
	}

} //StudyProgramModelFactoryImpl
