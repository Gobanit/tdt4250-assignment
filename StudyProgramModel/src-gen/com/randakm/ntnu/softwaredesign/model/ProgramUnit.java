/**
 */
package com.randakm.ntnu.softwaredesign.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Program Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getProgramUnit()
 * @model abstract="true"
 * @generated
 */
public interface ProgramUnit extends EObject {
} // ProgramUnit
