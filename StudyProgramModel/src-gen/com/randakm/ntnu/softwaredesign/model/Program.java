/**
 */
package com.randakm.ntnu.softwaredesign.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Program</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Program#getName <em>Name</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Program#getDescription <em>Description</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Program#getProgramUnits <em>Program Units</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Program#getUniversity <em>University</em>}</li>
 * </ul>
 *
 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getProgram()
 * @model
 * @generated
 */
public interface Program extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getProgram_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.Program#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getProgram_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.Program#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Program Units</b></em>' containment reference list.
	 * The list contents are of type {@link com.randakm.ntnu.softwaredesign.model.ProgramUnit}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Program Units</em>' containment reference list.
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getProgram_ProgramUnits()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProgramUnit> getProgramUnits();

	/**
	 * Returns the value of the '<em><b>University</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.randakm.ntnu.softwaredesign.model.University#getPrograms <em>Programs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>University</em>' container reference.
	 * @see #setUniversity(University)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getProgram_University()
	 * @see com.randakm.ntnu.softwaredesign.model.University#getPrograms
	 * @model opposite="programs" transient="false"
	 * @generated
	 */
	University getUniversity();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.Program#getUniversity <em>University</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>University</em>' container reference.
	 * @see #getUniversity()
	 * @generated
	 */
	void setUniversity(University value);

} // Program
