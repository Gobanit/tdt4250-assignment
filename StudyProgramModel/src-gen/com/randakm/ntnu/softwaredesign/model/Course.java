/**
 */
package com.randakm.ntnu.softwaredesign.model;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Course#getName <em>Name</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Course#getCode <em>Code</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Course#getDescription <em>Description</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Course#getCredits <em>Credits</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Course#getSemester <em>Semester</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Course#getAllCourses <em>All Courses</em>}</li>
 * </ul>
 *
 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getCourse()
 * @model
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getCourse_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getCourse_Code()
	 * @model
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.Course#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getCourse_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.Course#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits</em>' attribute.
	 * @see #setCredits(float)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getCourse_Credits()
	 * @model
	 * @generated
	 */
	float getCredits();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.Course#getCredits <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits</em>' attribute.
	 * @see #getCredits()
	 * @generated
	 */
	void setCredits(float value);

	/**
	 * Returns the value of the '<em><b>Semester</b></em>' attribute.
	 * The literals are from the enumeration {@link com.randakm.ntnu.softwaredesign.model.SemesterType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester</em>' attribute.
	 * @see com.randakm.ntnu.softwaredesign.model.SemesterType
	 * @see #setSemester(SemesterType)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getCourse_Semester()
	 * @model required="true"
	 * @generated
	 */
	SemesterType getSemester();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.Course#getSemester <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester</em>' attribute.
	 * @see com.randakm.ntnu.softwaredesign.model.SemesterType
	 * @see #getSemester()
	 * @generated
	 */
	void setSemester(SemesterType value);

	/**
	 * Returns the value of the '<em><b>All Courses</b></em>' reference list.
	 * The list contents are of type {@link com.randakm.ntnu.softwaredesign.model.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Courses</em>' reference list.
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getCourse_AllCourses()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Course> getAllCourses();

} // Course
