/**
 */
package com.randakm.ntnu.softwaredesign.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Semester#getSemesterType <em>Semester Type</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Semester#getMandatoryCourses <em>Mandatory Courses</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Semester#getElectiveCourses <em>Elective Courses</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Semester#getElectiveCourseGroups <em>Elective Course Groups</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Semester#getAllCourses <em>All Courses</em>}</li>
 * </ul>
 *
 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getSemester()
 * @model
 * @generated
 */
public interface Semester extends EObject {
	/**
	 * Returns the value of the '<em><b>Semester Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.randakm.ntnu.softwaredesign.model.SemesterType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester Type</em>' attribute.
	 * @see com.randakm.ntnu.softwaredesign.model.SemesterType
	 * @see #setSemesterType(SemesterType)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getSemester_SemesterType()
	 * @model required="true"
	 * @generated
	 */
	SemesterType getSemesterType();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.Semester#getSemesterType <em>Semester Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester Type</em>' attribute.
	 * @see com.randakm.ntnu.softwaredesign.model.SemesterType
	 * @see #getSemesterType()
	 * @generated
	 */
	void setSemesterType(SemesterType value);

	/**
	 * Returns the value of the '<em><b>Mandatory Courses</b></em>' reference list.
	 * The list contents are of type {@link com.randakm.ntnu.softwaredesign.model.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mandatory Courses</em>' reference list.
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getSemester_MandatoryCourses()
	 * @model
	 * @generated
	 */
	EList<Course> getMandatoryCourses();

	/**
	 * Returns the value of the '<em><b>Elective Courses</b></em>' reference list.
	 * The list contents are of type {@link com.randakm.ntnu.softwaredesign.model.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elective Courses</em>' reference list.
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getSemester_ElectiveCourses()
	 * @model
	 * @generated
	 */
	EList<Course> getElectiveCourses();

	/**
	 * Returns the value of the '<em><b>Elective Course Groups</b></em>' containment reference list.
	 * The list contents are of type {@link com.randakm.ntnu.softwaredesign.model.ElectiveGroup}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elective Course Groups</em>' containment reference list.
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getSemester_ElectiveCourseGroups()
	 * @model containment="true"
	 * @generated
	 */
	EList<ElectiveGroup> getElectiveCourseGroups();

	/**
	 * Returns the value of the '<em><b>All Courses</b></em>' reference list.
	 * The list contents are of type {@link com.randakm.ntnu.softwaredesign.model.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Courses</em>' reference list.
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getSemester_AllCourses()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Course> getAllCourses();

} // Semester
