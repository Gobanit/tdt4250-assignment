/**
 */
package com.randakm.ntnu.softwaredesign.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Elective Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.ElectiveGroup#getName <em>Name</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.ElectiveGroup#getRequired <em>Required</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.ElectiveGroup#getCourses <em>Courses</em>}</li>
 * </ul>
 *
 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getElectiveGroup()
 * @model
 * @generated
 */
public interface ElectiveGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getElectiveGroup_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.ElectiveGroup#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required</em>' attribute.
	 * @see #setRequired(int)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getElectiveGroup_Required()
	 * @model
	 * @generated
	 */
	int getRequired();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.ElectiveGroup#getRequired <em>Required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required</em>' attribute.
	 * @see #getRequired()
	 * @generated
	 */
	void setRequired(int value);

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' reference list.
	 * The list contents are of type {@link com.randakm.ntnu.softwaredesign.model.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' reference list.
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getElectiveGroup_Courses()
	 * @model
	 * @generated
	 */
	EList<Course> getCourses();

} // ElectiveGroup
