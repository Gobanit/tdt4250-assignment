/**
 */
package com.randakm.ntnu.softwaredesign.model;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL'"
 * @generated
 */
public interface StudyProgramModelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "model";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/studyProgramModel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "model";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StudyProgramModelPackage eINSTANCE = com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.randakm.ntnu.softwaredesign.model.impl.UniversityImpl <em>University</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.randakm.ntnu.softwaredesign.model.impl.UniversityImpl
	 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getUniversity()
	 * @generated
	 */
	int UNIVERSITY = 0;

	/**
	 * The feature id for the '<em><b>Catalogue</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY__CATALOGUE = 0;

	/**
	 * The feature id for the '<em><b>Programs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY__PROGRAMS = 1;

	/**
	 * The feature id for the '<em><b>Specialisation Catalogue</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY__SPECIALISATION_CATALOGUE = 2;

	/**
	 * The number of structural features of the '<em>University</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>University</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.randakm.ntnu.softwaredesign.model.impl.ProgramImpl <em>Program</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.randakm.ntnu.softwaredesign.model.impl.ProgramImpl
	 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getProgram()
	 * @generated
	 */
	int PROGRAM = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM__NAME = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Program Units</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM__PROGRAM_UNITS = 2;

	/**
	 * The feature id for the '<em><b>University</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM__UNIVERSITY = 3;

	/**
	 * The number of structural features of the '<em>Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.randakm.ntnu.softwaredesign.model.impl.CourseCatalogueImpl <em>Course Catalogue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.randakm.ntnu.softwaredesign.model.impl.CourseCatalogueImpl
	 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getCourseCatalogue()
	 * @generated
	 */
	int COURSE_CATALOGUE = 2;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_CATALOGUE__COURSES = 0;

	/**
	 * The number of structural features of the '<em>Course Catalogue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_CATALOGUE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Course Catalogue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_CATALOGUE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.randakm.ntnu.softwaredesign.model.impl.SpecialisationCatalogueImpl <em>Specialisation Catalogue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.randakm.ntnu.softwaredesign.model.impl.SpecialisationCatalogueImpl
	 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getSpecialisationCatalogue()
	 * @generated
	 */
	int SPECIALISATION_CATALOGUE = 3;

	/**
	 * The feature id for the '<em><b>Specialisations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALISATION_CATALOGUE__SPECIALISATIONS = 0;

	/**
	 * The number of structural features of the '<em>Specialisation Catalogue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALISATION_CATALOGUE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Specialisation Catalogue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALISATION_CATALOGUE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.randakm.ntnu.softwaredesign.model.impl.ProgramUnitImpl <em>Program Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.randakm.ntnu.softwaredesign.model.impl.ProgramUnitImpl
	 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getProgramUnit()
	 * @generated
	 */
	int PROGRAM_UNIT = 4;

	/**
	 * The number of structural features of the '<em>Program Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM_UNIT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Program Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM_UNIT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.randakm.ntnu.softwaredesign.model.impl.SpecialisationChooserImpl <em>Specialisation Chooser</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.randakm.ntnu.softwaredesign.model.impl.SpecialisationChooserImpl
	 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getSpecialisationChooser()
	 * @generated
	 */
	int SPECIALISATION_CHOOSER = 5;

	/**
	 * The feature id for the '<em><b>Available Specialisations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALISATION_CHOOSER__AVAILABLE_SPECIALISATIONS = PROGRAM_UNIT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Specialisation Chooser</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALISATION_CHOOSER_FEATURE_COUNT = PROGRAM_UNIT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Specialisation Chooser</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALISATION_CHOOSER_OPERATION_COUNT = PROGRAM_UNIT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.randakm.ntnu.softwaredesign.model.impl.StudyYearImpl <em>Study Year</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyYearImpl
	 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getStudyYear()
	 * @generated
	 */
	int STUDY_YEAR = 6;

	/**
	 * The feature id for the '<em><b>Autumm</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_YEAR__AUTUMM = PROGRAM_UNIT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Spring</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_YEAR__SPRING = PROGRAM_UNIT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Study Year</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_YEAR_FEATURE_COUNT = PROGRAM_UNIT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Study Year</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_YEAR_OPERATION_COUNT = PROGRAM_UNIT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.randakm.ntnu.softwaredesign.model.impl.SpecialisationImpl <em>Specialisation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.randakm.ntnu.softwaredesign.model.impl.SpecialisationImpl
	 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getSpecialisation()
	 * @generated
	 */
	int SPECIALISATION = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALISATION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Program Units</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALISATION__PROGRAM_UNITS = 1;

	/**
	 * The number of structural features of the '<em>Specialisation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALISATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Specialisation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALISATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.randakm.ntnu.softwaredesign.model.impl.SemesterImpl <em>Semester</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.randakm.ntnu.softwaredesign.model.impl.SemesterImpl
	 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getSemester()
	 * @generated
	 */
	int SEMESTER = 8;

	/**
	 * The feature id for the '<em><b>Semester Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__SEMESTER_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Mandatory Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__MANDATORY_COURSES = 1;

	/**
	 * The feature id for the '<em><b>Elective Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__ELECTIVE_COURSES = 2;

	/**
	 * The feature id for the '<em><b>Elective Course Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__ELECTIVE_COURSE_GROUPS = 3;

	/**
	 * The feature id for the '<em><b>All Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__ALL_COURSES = 4;

	/**
	 * The number of structural features of the '<em>Semester</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Semester</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.randakm.ntnu.softwaredesign.model.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.randakm.ntnu.softwaredesign.model.impl.CourseImpl
	 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CODE = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDITS = 3;

	/**
	 * The feature id for the '<em><b>Semester</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__SEMESTER = 4;

	/**
	 * The feature id for the '<em><b>All Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__ALL_COURSES = 5;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.randakm.ntnu.softwaredesign.model.impl.ElectiveGroupImpl <em>Elective Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.randakm.ntnu.softwaredesign.model.impl.ElectiveGroupImpl
	 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getElectiveGroup()
	 * @generated
	 */
	int ELECTIVE_GROUP = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTIVE_GROUP__NAME = 0;

	/**
	 * The feature id for the '<em><b>Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTIVE_GROUP__REQUIRED = 1;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTIVE_GROUP__COURSES = 2;

	/**
	 * The number of structural features of the '<em>Elective Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTIVE_GROUP_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Elective Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTIVE_GROUP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.randakm.ntnu.softwaredesign.model.SemesterType <em>Semester Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.randakm.ntnu.softwaredesign.model.SemesterType
	 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getSemesterType()
	 * @generated
	 */
	int SEMESTER_TYPE = 11;

	/**
	 * Returns the meta object for class '{@link com.randakm.ntnu.softwaredesign.model.University <em>University</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>University</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.University
	 * @generated
	 */
	EClass getUniversity();

	/**
	 * Returns the meta object for the containment reference '{@link com.randakm.ntnu.softwaredesign.model.University#getCatalogue <em>Catalogue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Catalogue</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.University#getCatalogue()
	 * @see #getUniversity()
	 * @generated
	 */
	EReference getUniversity_Catalogue();

	/**
	 * Returns the meta object for the containment reference list '{@link com.randakm.ntnu.softwaredesign.model.University#getPrograms <em>Programs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Programs</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.University#getPrograms()
	 * @see #getUniversity()
	 * @generated
	 */
	EReference getUniversity_Programs();

	/**
	 * Returns the meta object for the containment reference '{@link com.randakm.ntnu.softwaredesign.model.University#getSpecialisationCatalogue <em>Specialisation Catalogue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Specialisation Catalogue</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.University#getSpecialisationCatalogue()
	 * @see #getUniversity()
	 * @generated
	 */
	EReference getUniversity_SpecialisationCatalogue();

	/**
	 * Returns the meta object for class '{@link com.randakm.ntnu.softwaredesign.model.Program <em>Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Program</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Program
	 * @generated
	 */
	EClass getProgram();

	/**
	 * Returns the meta object for the attribute '{@link com.randakm.ntnu.softwaredesign.model.Program#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Program#getName()
	 * @see #getProgram()
	 * @generated
	 */
	EAttribute getProgram_Name();

	/**
	 * Returns the meta object for the attribute '{@link com.randakm.ntnu.softwaredesign.model.Program#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Program#getDescription()
	 * @see #getProgram()
	 * @generated
	 */
	EAttribute getProgram_Description();

	/**
	 * Returns the meta object for the containment reference list '{@link com.randakm.ntnu.softwaredesign.model.Program#getProgramUnits <em>Program Units</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Program Units</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Program#getProgramUnits()
	 * @see #getProgram()
	 * @generated
	 */
	EReference getProgram_ProgramUnits();

	/**
	 * Returns the meta object for the container reference '{@link com.randakm.ntnu.softwaredesign.model.Program#getUniversity <em>University</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>University</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Program#getUniversity()
	 * @see #getProgram()
	 * @generated
	 */
	EReference getProgram_University();

	/**
	 * Returns the meta object for class '{@link com.randakm.ntnu.softwaredesign.model.CourseCatalogue <em>Course Catalogue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Catalogue</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.CourseCatalogue
	 * @generated
	 */
	EClass getCourseCatalogue();

	/**
	 * Returns the meta object for the containment reference list '{@link com.randakm.ntnu.softwaredesign.model.CourseCatalogue#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Courses</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.CourseCatalogue#getCourses()
	 * @see #getCourseCatalogue()
	 * @generated
	 */
	EReference getCourseCatalogue_Courses();

	/**
	 * Returns the meta object for class '{@link com.randakm.ntnu.softwaredesign.model.SpecialisationCatalogue <em>Specialisation Catalogue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Specialisation Catalogue</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.SpecialisationCatalogue
	 * @generated
	 */
	EClass getSpecialisationCatalogue();

	/**
	 * Returns the meta object for the containment reference list '{@link com.randakm.ntnu.softwaredesign.model.SpecialisationCatalogue#getSpecialisations <em>Specialisations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Specialisations</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.SpecialisationCatalogue#getSpecialisations()
	 * @see #getSpecialisationCatalogue()
	 * @generated
	 */
	EReference getSpecialisationCatalogue_Specialisations();

	/**
	 * Returns the meta object for class '{@link com.randakm.ntnu.softwaredesign.model.ProgramUnit <em>Program Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Program Unit</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.ProgramUnit
	 * @generated
	 */
	EClass getProgramUnit();

	/**
	 * Returns the meta object for class '{@link com.randakm.ntnu.softwaredesign.model.SpecialisationChooser <em>Specialisation Chooser</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Specialisation Chooser</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.SpecialisationChooser
	 * @generated
	 */
	EClass getSpecialisationChooser();

	/**
	 * Returns the meta object for the reference list '{@link com.randakm.ntnu.softwaredesign.model.SpecialisationChooser#getAvailableSpecialisations <em>Available Specialisations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Available Specialisations</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.SpecialisationChooser#getAvailableSpecialisations()
	 * @see #getSpecialisationChooser()
	 * @generated
	 */
	EReference getSpecialisationChooser_AvailableSpecialisations();

	/**
	 * Returns the meta object for class '{@link com.randakm.ntnu.softwaredesign.model.StudyYear <em>Study Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Study Year</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.StudyYear
	 * @generated
	 */
	EClass getStudyYear();

	/**
	 * Returns the meta object for the containment reference '{@link com.randakm.ntnu.softwaredesign.model.StudyYear#getAutumm <em>Autumm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Autumm</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.StudyYear#getAutumm()
	 * @see #getStudyYear()
	 * @generated
	 */
	EReference getStudyYear_Autumm();

	/**
	 * Returns the meta object for the containment reference '{@link com.randakm.ntnu.softwaredesign.model.StudyYear#getSpring <em>Spring</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Spring</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.StudyYear#getSpring()
	 * @see #getStudyYear()
	 * @generated
	 */
	EReference getStudyYear_Spring();

	/**
	 * Returns the meta object for class '{@link com.randakm.ntnu.softwaredesign.model.Specialisation <em>Specialisation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Specialisation</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Specialisation
	 * @generated
	 */
	EClass getSpecialisation();

	/**
	 * Returns the meta object for the attribute '{@link com.randakm.ntnu.softwaredesign.model.Specialisation#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Specialisation#getName()
	 * @see #getSpecialisation()
	 * @generated
	 */
	EAttribute getSpecialisation_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link com.randakm.ntnu.softwaredesign.model.Specialisation#getProgramUnits <em>Program Units</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Program Units</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Specialisation#getProgramUnits()
	 * @see #getSpecialisation()
	 * @generated
	 */
	EReference getSpecialisation_ProgramUnits();

	/**
	 * Returns the meta object for class '{@link com.randakm.ntnu.softwaredesign.model.Semester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Semester</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Semester
	 * @generated
	 */
	EClass getSemester();

	/**
	 * Returns the meta object for the attribute '{@link com.randakm.ntnu.softwaredesign.model.Semester#getSemesterType <em>Semester Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Semester Type</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Semester#getSemesterType()
	 * @see #getSemester()
	 * @generated
	 */
	EAttribute getSemester_SemesterType();

	/**
	 * Returns the meta object for the reference list '{@link com.randakm.ntnu.softwaredesign.model.Semester#getMandatoryCourses <em>Mandatory Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Mandatory Courses</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Semester#getMandatoryCourses()
	 * @see #getSemester()
	 * @generated
	 */
	EReference getSemester_MandatoryCourses();

	/**
	 * Returns the meta object for the reference list '{@link com.randakm.ntnu.softwaredesign.model.Semester#getElectiveCourses <em>Elective Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Elective Courses</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Semester#getElectiveCourses()
	 * @see #getSemester()
	 * @generated
	 */
	EReference getSemester_ElectiveCourses();

	/**
	 * Returns the meta object for the containment reference list '{@link com.randakm.ntnu.softwaredesign.model.Semester#getElectiveCourseGroups <em>Elective Course Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elective Course Groups</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Semester#getElectiveCourseGroups()
	 * @see #getSemester()
	 * @generated
	 */
	EReference getSemester_ElectiveCourseGroups();

	/**
	 * Returns the meta object for the reference list '{@link com.randakm.ntnu.softwaredesign.model.Semester#getAllCourses <em>All Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Courses</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Semester#getAllCourses()
	 * @see #getSemester()
	 * @generated
	 */
	EReference getSemester_AllCourses();

	/**
	 * Returns the meta object for class '{@link com.randakm.ntnu.softwaredesign.model.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the attribute '{@link com.randakm.ntnu.softwaredesign.model.Course#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Course#getName()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Name();

	/**
	 * Returns the meta object for the attribute '{@link com.randakm.ntnu.softwaredesign.model.Course#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Course#getCode()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Code();

	/**
	 * Returns the meta object for the attribute '{@link com.randakm.ntnu.softwaredesign.model.Course#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Course#getDescription()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Description();

	/**
	 * Returns the meta object for the attribute '{@link com.randakm.ntnu.softwaredesign.model.Course#getCredits <em>Credits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Course#getCredits()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Credits();

	/**
	 * Returns the meta object for the attribute '{@link com.randakm.ntnu.softwaredesign.model.Course#getSemester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Semester</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Course#getSemester()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Semester();

	/**
	 * Returns the meta object for the reference list '{@link com.randakm.ntnu.softwaredesign.model.Course#getAllCourses <em>All Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Courses</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.Course#getAllCourses()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_AllCourses();

	/**
	 * Returns the meta object for class '{@link com.randakm.ntnu.softwaredesign.model.ElectiveGroup <em>Elective Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Elective Group</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.ElectiveGroup
	 * @generated
	 */
	EClass getElectiveGroup();

	/**
	 * Returns the meta object for the attribute '{@link com.randakm.ntnu.softwaredesign.model.ElectiveGroup#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.ElectiveGroup#getName()
	 * @see #getElectiveGroup()
	 * @generated
	 */
	EAttribute getElectiveGroup_Name();

	/**
	 * Returns the meta object for the attribute '{@link com.randakm.ntnu.softwaredesign.model.ElectiveGroup#getRequired <em>Required</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Required</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.ElectiveGroup#getRequired()
	 * @see #getElectiveGroup()
	 * @generated
	 */
	EAttribute getElectiveGroup_Required();

	/**
	 * Returns the meta object for the reference list '{@link com.randakm.ntnu.softwaredesign.model.ElectiveGroup#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Courses</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.ElectiveGroup#getCourses()
	 * @see #getElectiveGroup()
	 * @generated
	 */
	EReference getElectiveGroup_Courses();

	/**
	 * Returns the meta object for enum '{@link com.randakm.ntnu.softwaredesign.model.SemesterType <em>Semester Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Semester Type</em>'.
	 * @see com.randakm.ntnu.softwaredesign.model.SemesterType
	 * @generated
	 */
	EEnum getSemesterType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StudyProgramModelFactory getStudyProgramModelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.randakm.ntnu.softwaredesign.model.impl.UniversityImpl <em>University</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.randakm.ntnu.softwaredesign.model.impl.UniversityImpl
		 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getUniversity()
		 * @generated
		 */
		EClass UNIVERSITY = eINSTANCE.getUniversity();

		/**
		 * The meta object literal for the '<em><b>Catalogue</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIVERSITY__CATALOGUE = eINSTANCE.getUniversity_Catalogue();

		/**
		 * The meta object literal for the '<em><b>Programs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIVERSITY__PROGRAMS = eINSTANCE.getUniversity_Programs();

		/**
		 * The meta object literal for the '<em><b>Specialisation Catalogue</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIVERSITY__SPECIALISATION_CATALOGUE = eINSTANCE.getUniversity_SpecialisationCatalogue();

		/**
		 * The meta object literal for the '{@link com.randakm.ntnu.softwaredesign.model.impl.ProgramImpl <em>Program</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.randakm.ntnu.softwaredesign.model.impl.ProgramImpl
		 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getProgram()
		 * @generated
		 */
		EClass PROGRAM = eINSTANCE.getProgram();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROGRAM__NAME = eINSTANCE.getProgram_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROGRAM__DESCRIPTION = eINSTANCE.getProgram_Description();

		/**
		 * The meta object literal for the '<em><b>Program Units</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROGRAM__PROGRAM_UNITS = eINSTANCE.getProgram_ProgramUnits();

		/**
		 * The meta object literal for the '<em><b>University</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROGRAM__UNIVERSITY = eINSTANCE.getProgram_University();

		/**
		 * The meta object literal for the '{@link com.randakm.ntnu.softwaredesign.model.impl.CourseCatalogueImpl <em>Course Catalogue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.randakm.ntnu.softwaredesign.model.impl.CourseCatalogueImpl
		 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getCourseCatalogue()
		 * @generated
		 */
		EClass COURSE_CATALOGUE = eINSTANCE.getCourseCatalogue();

		/**
		 * The meta object literal for the '<em><b>Courses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_CATALOGUE__COURSES = eINSTANCE.getCourseCatalogue_Courses();

		/**
		 * The meta object literal for the '{@link com.randakm.ntnu.softwaredesign.model.impl.SpecialisationCatalogueImpl <em>Specialisation Catalogue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.randakm.ntnu.softwaredesign.model.impl.SpecialisationCatalogueImpl
		 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getSpecialisationCatalogue()
		 * @generated
		 */
		EClass SPECIALISATION_CATALOGUE = eINSTANCE.getSpecialisationCatalogue();

		/**
		 * The meta object literal for the '<em><b>Specialisations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SPECIALISATION_CATALOGUE__SPECIALISATIONS = eINSTANCE.getSpecialisationCatalogue_Specialisations();

		/**
		 * The meta object literal for the '{@link com.randakm.ntnu.softwaredesign.model.impl.ProgramUnitImpl <em>Program Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.randakm.ntnu.softwaredesign.model.impl.ProgramUnitImpl
		 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getProgramUnit()
		 * @generated
		 */
		EClass PROGRAM_UNIT = eINSTANCE.getProgramUnit();

		/**
		 * The meta object literal for the '{@link com.randakm.ntnu.softwaredesign.model.impl.SpecialisationChooserImpl <em>Specialisation Chooser</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.randakm.ntnu.softwaredesign.model.impl.SpecialisationChooserImpl
		 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getSpecialisationChooser()
		 * @generated
		 */
		EClass SPECIALISATION_CHOOSER = eINSTANCE.getSpecialisationChooser();

		/**
		 * The meta object literal for the '<em><b>Available Specialisations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SPECIALISATION_CHOOSER__AVAILABLE_SPECIALISATIONS = eINSTANCE
				.getSpecialisationChooser_AvailableSpecialisations();

		/**
		 * The meta object literal for the '{@link com.randakm.ntnu.softwaredesign.model.impl.StudyYearImpl <em>Study Year</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyYearImpl
		 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getStudyYear()
		 * @generated
		 */
		EClass STUDY_YEAR = eINSTANCE.getStudyYear();

		/**
		 * The meta object literal for the '<em><b>Autumm</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDY_YEAR__AUTUMM = eINSTANCE.getStudyYear_Autumm();

		/**
		 * The meta object literal for the '<em><b>Spring</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDY_YEAR__SPRING = eINSTANCE.getStudyYear_Spring();

		/**
		 * The meta object literal for the '{@link com.randakm.ntnu.softwaredesign.model.impl.SpecialisationImpl <em>Specialisation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.randakm.ntnu.softwaredesign.model.impl.SpecialisationImpl
		 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getSpecialisation()
		 * @generated
		 */
		EClass SPECIALISATION = eINSTANCE.getSpecialisation();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPECIALISATION__NAME = eINSTANCE.getSpecialisation_Name();

		/**
		 * The meta object literal for the '<em><b>Program Units</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SPECIALISATION__PROGRAM_UNITS = eINSTANCE.getSpecialisation_ProgramUnits();

		/**
		 * The meta object literal for the '{@link com.randakm.ntnu.softwaredesign.model.impl.SemesterImpl <em>Semester</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.randakm.ntnu.softwaredesign.model.impl.SemesterImpl
		 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getSemester()
		 * @generated
		 */
		EClass SEMESTER = eINSTANCE.getSemester();

		/**
		 * The meta object literal for the '<em><b>Semester Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEMESTER__SEMESTER_TYPE = eINSTANCE.getSemester_SemesterType();

		/**
		 * The meta object literal for the '<em><b>Mandatory Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEMESTER__MANDATORY_COURSES = eINSTANCE.getSemester_MandatoryCourses();

		/**
		 * The meta object literal for the '<em><b>Elective Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEMESTER__ELECTIVE_COURSES = eINSTANCE.getSemester_ElectiveCourses();

		/**
		 * The meta object literal for the '<em><b>Elective Course Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEMESTER__ELECTIVE_COURSE_GROUPS = eINSTANCE.getSemester_ElectiveCourseGroups();

		/**
		 * The meta object literal for the '<em><b>All Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEMESTER__ALL_COURSES = eINSTANCE.getSemester_AllCourses();

		/**
		 * The meta object literal for the '{@link com.randakm.ntnu.softwaredesign.model.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.randakm.ntnu.softwaredesign.model.impl.CourseImpl
		 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__NAME = eINSTANCE.getCourse_Name();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CODE = eINSTANCE.getCourse_Code();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__DESCRIPTION = eINSTANCE.getCourse_Description();

		/**
		 * The meta object literal for the '<em><b>Credits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CREDITS = eINSTANCE.getCourse_Credits();

		/**
		 * The meta object literal for the '<em><b>Semester</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__SEMESTER = eINSTANCE.getCourse_Semester();

		/**
		 * The meta object literal for the '<em><b>All Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__ALL_COURSES = eINSTANCE.getCourse_AllCourses();

		/**
		 * The meta object literal for the '{@link com.randakm.ntnu.softwaredesign.model.impl.ElectiveGroupImpl <em>Elective Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.randakm.ntnu.softwaredesign.model.impl.ElectiveGroupImpl
		 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getElectiveGroup()
		 * @generated
		 */
		EClass ELECTIVE_GROUP = eINSTANCE.getElectiveGroup();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELECTIVE_GROUP__NAME = eINSTANCE.getElectiveGroup_Name();

		/**
		 * The meta object literal for the '<em><b>Required</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELECTIVE_GROUP__REQUIRED = eINSTANCE.getElectiveGroup_Required();

		/**
		 * The meta object literal for the '<em><b>Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELECTIVE_GROUP__COURSES = eINSTANCE.getElectiveGroup_Courses();

		/**
		 * The meta object literal for the '{@link com.randakm.ntnu.softwaredesign.model.SemesterType <em>Semester Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.randakm.ntnu.softwaredesign.model.SemesterType
		 * @see com.randakm.ntnu.softwaredesign.model.impl.StudyProgramModelPackageImpl#getSemesterType()
		 * @generated
		 */
		EEnum SEMESTER_TYPE = eINSTANCE.getSemesterType();

	}

} //StudyProgramModelPackage
