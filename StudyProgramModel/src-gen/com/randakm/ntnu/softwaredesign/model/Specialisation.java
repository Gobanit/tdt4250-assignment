/**
 */
package com.randakm.ntnu.softwaredesign.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Specialisation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Specialisation#getName <em>Name</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.Specialisation#getProgramUnits <em>Program Units</em>}</li>
 * </ul>
 *
 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getSpecialisation()
 * @model
 * @generated
 */
public interface Specialisation extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getSpecialisation_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.Specialisation#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Program Units</b></em>' containment reference list.
	 * The list contents are of type {@link com.randakm.ntnu.softwaredesign.model.ProgramUnit}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Program Units</em>' containment reference list.
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getSpecialisation_ProgramUnits()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProgramUnit> getProgramUnits();

} // Specialisation
