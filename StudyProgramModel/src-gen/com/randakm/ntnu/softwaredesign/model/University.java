/**
 */
package com.randakm.ntnu.softwaredesign.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>University</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.University#getCatalogue <em>Catalogue</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.University#getPrograms <em>Programs</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.University#getSpecialisationCatalogue <em>Specialisation Catalogue</em>}</li>
 * </ul>
 *
 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getUniversity()
 * @model
 * @generated
 */
public interface University extends EObject {
	/**
	 * Returns the value of the '<em><b>Catalogue</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Catalogue</em>' containment reference.
	 * @see #setCatalogue(CourseCatalogue)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getUniversity_Catalogue()
	 * @model containment="true" required="true"
	 * @generated
	 */
	CourseCatalogue getCatalogue();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.University#getCatalogue <em>Catalogue</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Catalogue</em>' containment reference.
	 * @see #getCatalogue()
	 * @generated
	 */
	void setCatalogue(CourseCatalogue value);

	/**
	 * Returns the value of the '<em><b>Programs</b></em>' containment reference list.
	 * The list contents are of type {@link com.randakm.ntnu.softwaredesign.model.Program}.
	 * It is bidirectional and its opposite is '{@link com.randakm.ntnu.softwaredesign.model.Program#getUniversity <em>University</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Programs</em>' containment reference list.
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getUniversity_Programs()
	 * @see com.randakm.ntnu.softwaredesign.model.Program#getUniversity
	 * @model opposite="university" containment="true"
	 * @generated
	 */
	EList<Program> getPrograms();

	/**
	 * Returns the value of the '<em><b>Specialisation Catalogue</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialisation Catalogue</em>' containment reference.
	 * @see #setSpecialisationCatalogue(SpecialisationCatalogue)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getUniversity_SpecialisationCatalogue()
	 * @model containment="true" required="true"
	 * @generated
	 */
	SpecialisationCatalogue getSpecialisationCatalogue();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.University#getSpecialisationCatalogue <em>Specialisation Catalogue</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specialisation Catalogue</em>' containment reference.
	 * @see #getSpecialisationCatalogue()
	 * @generated
	 */
	void setSpecialisationCatalogue(SpecialisationCatalogue value);

} // University
