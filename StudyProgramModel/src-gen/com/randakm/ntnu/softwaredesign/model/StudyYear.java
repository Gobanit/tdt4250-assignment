/**
 */
package com.randakm.ntnu.softwaredesign.model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Study Year</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.StudyYear#getAutumm <em>Autumm</em>}</li>
 *   <li>{@link com.randakm.ntnu.softwaredesign.model.StudyYear#getSpring <em>Spring</em>}</li>
 * </ul>
 *
 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getStudyYear()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='autummSemesterType springSemesterType'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL autummSemesterType='self.autumm.semesterType=SemesterType::Autumm' springSemesterType='self.spring.semesterType=SemesterType::Spring'"
 * @generated
 */
public interface StudyYear extends ProgramUnit {
	/**
	 * Returns the value of the '<em><b>Autumm</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Autumm</em>' containment reference.
	 * @see #setAutumm(Semester)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getStudyYear_Autumm()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Semester getAutumm();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.StudyYear#getAutumm <em>Autumm</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Autumm</em>' containment reference.
	 * @see #getAutumm()
	 * @generated
	 */
	void setAutumm(Semester value);

	/**
	 * Returns the value of the '<em><b>Spring</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Spring</em>' containment reference.
	 * @see #setSpring(Semester)
	 * @see com.randakm.ntnu.softwaredesign.model.StudyProgramModelPackage#getStudyYear_Spring()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Semester getSpring();

	/**
	 * Sets the value of the '{@link com.randakm.ntnu.softwaredesign.model.StudyYear#getSpring <em>Spring</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Spring</em>' containment reference.
	 * @see #getSpring()
	 * @generated
	 */
	void setSpring(Semester value);

} // StudyYear
